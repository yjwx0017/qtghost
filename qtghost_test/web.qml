import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.13
import QtWebEngine 1.9
import QtQml 2.13

ApplicationWindow {
    visible: true
    width: 600
    height: 400
    title: "Ghosts around"

    WebEngineView {
        anchors.fill: parent
        url: "https://www.baidu.com"
    }
}
